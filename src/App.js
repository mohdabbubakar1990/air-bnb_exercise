import { useEffect, useCallback, useState } from "react";
import UserList from "./components/UserList";
import ChatContainer from "./components/ChatContainer";
import "./App.css";
import LoadingComponent from "./components/Loading";
const loadChatInfo = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      fetch("data.json")
        .then((res) => res.json())
        .then((res) => {
          resolve(res.data);
        });
    }, 1000);
  });
};
const filterUser = (chats) => {
  const users = new Set();
  chats.forEach((chat) => {
    users.add(chat.from);
  });
  return [...users];
};
const filterChat = (chats, user) => {
  const message = [];
  chats.forEach((chat) => {
    if (
      (chat.from === user && chat.to === loginUser) ||
      (chat.to === user && chat.from === loginUser)
    )
      message.push(chat);
  });
  return message;
};
const loginUser = "test";
function App() {
  const [isLoading, setIsLoading] = useState(false);
  const [chatHistory, setChatHistory] = useState(null);
  const [currentUser, setCurrentUser] = useState(null);
  const [chats, setChats] = useState([]);
  const [userList, setUserList] = useState([]);

  useEffect(() => {
    const loadUserData = async () => {
      setIsLoading(true);
      const data = await loadChatInfo();
      setChatHistory(data);
      const users = filterUser(data);
      setUserList(users);
      setCurrentUser(users[0]);
      const chat = filterChat(data, users[0]);
      setChats(chat);
      setIsLoading(false);
    };
    loadUserData();
  }, []);

  const massegeHandler = useCallback(
    (newMessage) => {
      setIsLoading(true);
      setTimeout(() => {
        const chatObj = { to: currentUser, msg: newMessage, from: loginUser };
        chats.push(chatObj);
        chatHistory.push(chatObj);
        setChats(chats);
        setChatHistory(chatHistory);
        setIsLoading(false);
      }, 500);
    },
    [chats, chatHistory, currentUser]
  );
  const updateUser = useCallback(
    (newUser) => {
      setCurrentUser(newUser);
      const chat = filterChat(chatHistory, newUser);
      setChats(chat);
    },
    [chatHistory]
  );
  return (
    <div className="container">
      <LoadingComponent isLoading={isLoading} />
      <header className="chat-header">Chat App</header>
      <div className="row">
        <div className="col col-3 full-height border-right">
          <UserList
            users={userList}
            updateUser={updateUser}
            currentUser={currentUser}
          />
        </div>
        <div className="col col-7 full-height">
          <ChatContainer
            chats={chats}
            massegeHandler={massegeHandler}
            currentUser={currentUser}
            loginUser={loginUser}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
