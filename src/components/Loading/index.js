import "./index.css";
const LoadingComponent = ({ isLoading }) => {
  return isLoading ? (
    <div className="loadingContainer">
      <div>Loading...</div>
    </div>
  ) : null;
};

export default LoadingComponent;
