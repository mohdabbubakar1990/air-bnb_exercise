import "./index.css";
import React, { useState, useCallback, useEffect } from "react";
const ChatContainer = ({ chats, massegeHandler, loginUser }) => {
  const [message, setMessage] = useState("");
  const ref = React.createRef();
  useEffect(() => {
    ref.current.scrollIntoView({ behavior: "smooth" });
  }, [ref]);

  const onChangeHandler = useCallback((event) => {
    setMessage(event.currentTarget.value);
  }, []);

  const onClickHandler = () => {
    massegeHandler(message);
    setMessage("");
  };
  return (
    <div className="chatWrapper">
      <div className="chats">
        {chats.map((chat, index) => {
          return (
            <div
              key={`${chat}+${index}`}
              className={`${
                chat.from === loginUser ? "my-message" : "other-message"
              }`}
            >
              <p>{chat.msg}</p>
            </div>
          );
        })}
        <div ref={ref}></div>
      </div>
      {chats.length > 0 && (
        <div className="inputWrapper">
          <input onChange={onChangeHandler} value={message} />
          <button onClick={onClickHandler}>send</button>
        </div>
      )}
    </div>
  );
};

export default ChatContainer;
