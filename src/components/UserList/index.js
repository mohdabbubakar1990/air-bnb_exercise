import "./index.css";

const UserList = ({ users, updateUser, currentUser, chatHistory }) => {
  return (
    <div className="userWrapper">
      {users.map((user) => {
        return (
          <div
            key={user}
            onClick={() => updateUser(user)}
            className={`${currentUser === user ? "active" : ""}`}
          >
            <p>{user}</p>
          </div>
        );
      })}
    </div>
  );
};
export default UserList;
